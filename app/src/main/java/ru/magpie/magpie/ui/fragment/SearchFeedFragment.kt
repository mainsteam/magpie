package ru.magpie.magpie.ui.fragment


import android.content.Context
import android.os.Bundle
import android.support.v4.content.ContextCompat.getColor
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import butterknife.*
import com.arellomobile.mvp.MvpAppCompatFragment

import ru.magpie.magpie.R
import ru.magpie.magpie.common.AnimUtils
import ru.magpie.magpie.common.MyApp
import ru.magpie.magpie.ui.adapter.FeedlySearchAdapter
import javax.inject.Inject
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import com.arellomobile.mvp.presenter.InjectPresenter
import ru.magpie.magpie.domain.models.Subscription
import ru.magpie.magpie.mvp.presenter.SearchFeedPresenter
import ru.magpie.magpie.mvp.view.SearchFeedView


class SearchFeedFragment : MvpAppCompatFragment(), SearchFeedView {
    @InjectPresenter lateinit var presenter: SearchFeedPresenter
    @Inject lateinit var aUtils: AnimUtils

    @BindView(R.id.search_recycler) lateinit var recyclerView: RecyclerView
    @BindView(R.id.search_preloader) lateinit var preloader: ProgressBar
    @BindView(R.id.clear_input_btn) lateinit var clearInputBtn: View
    @BindView(R.id.search_text) lateinit var searchText: TextView
    lateinit var adapter: FeedlySearchAdapter
    lateinit var unbinder: Unbinder
    lateinit var rootView: View


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        rootView = inflater!!.inflate(R.layout.fragment_search_feed, container, false)
        MyApp.appComponent.inject(this)
        unbinder = ButterKnife.bind(this, rootView)
        setUpRecycler()
        aUtils.registerRevealAnimation(context, rootView,
                getColor(context, R.color.colorAccent), getColor(context, R.color.colorWhite))
        return rootView
    }


    @OnEditorAction(R.id.search_text)
    fun searchFeedAction(actionId: Int): Boolean {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            preloader.visibility = View.VISIBLE
            hideKeyboard()
            presenter.searchFeed(searchText.text.toString())
            return true
        }
        return false
    }

    override fun onSearchFinished(feeds: MutableList<Subscription>) {
        preloader.visibility = View.GONE
        adapter.items = feeds
        adapter.notifyDataSetChanged()
    }

    @OnTextChanged(value = [(R.id.search_text)], callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    fun onInputChanged(e: Editable) {
        if (e.isNotEmpty()) {
            clearInputBtn.visibility = View.VISIBLE
        } else {
            clearInputBtn.visibility = View.GONE
        }
    }

    @OnClick(R.id.clear_input_btn)
    fun clearInput(view: View) {
        searchText.text = ""
    }

    private fun setUpRecycler() {
        recyclerView.layoutManager = LinearLayoutManager(context)
        adapter = FeedlySearchAdapter(context)
        recyclerView.adapter = adapter
    }


    private fun showKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, 0)
    }

    private fun hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
    }


    override fun onDestroyView() {
        super.onDestroyView()
        unbinder.unbind()
    }
}
