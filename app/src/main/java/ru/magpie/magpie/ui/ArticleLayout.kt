package ru.magpie.magpie.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.preference.PreferenceManager
import android.support.v4.content.ContextCompat.startActivity
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import ru.magpie.magpie.R
import ru.magpie.magpie.domain.models.Article
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import android.text.style.URLSpan
import android.text.SpannableStringBuilder
import android.text.style.ClickableSpan
import android.widget.*
import ru.magpie.magpie.common.HtmlHelper.Companion.IMG_URL_PATTERN
import ru.magpie.magpie.ui.activity.WebViewActivity
import ru.magpie.magpie.ui.activity.WebViewActivity.Companion.ARTICLE_TITLE
import ru.magpie.magpie.ui.activity.WebViewActivity.Companion.ARTICLE_URL
import ru.magpie.magpie.ui.activity.WebViewActivity.Companion.FROM_SECONDARY_LINK


class ArticleLayout : LinearLayout {
    private val IMAGE_MATCHER_GROUP = 2
    private val pattern = Pattern.compile("(http|https)://.*?\\.(jpeg|png|jpg)")
    private val sdf = SimpleDateFormat("MMM d, yyyy hh:mm aaa", Locale.US)
    private var defHorMargin: Int = resources.getDimensionPixelSize(R.dimen.def_hor_margin)
    private var defVerMargin: Int = resources.getDimensionPixelSize(R.dimen.def_ver_margin)
    private val textTileSize: Float = resources.getDimension(R.dimen.text_title_size)

    constructor(context: Context, art: Article) : super(context) {
        initLayout(context, art)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)


    private fun initLayout(context: Context?, art: Article) {
        orientation = LinearLayout.VERTICAL
        gravity = Gravity.CENTER_HORIZONTAL
        val linearParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)


        // Header
        val headerView = inflate(context, R.layout.layout_article_header, null)
        val articleTitle = headerView.findViewById<TextView>(R.id.article_title)
        val articleDate = headerView.findViewById<TextView>(R.id.article_dt)

        articleTitle.text = art.title
        articleDate.text = sdf.format(art.pubDate)
        addView(headerView)


        // Body
        val separatedDesc = mutableListOf<String>()
        val matcher = IMG_URL_PATTERN.matcher(art.description)

        var descCopy = art.description

        // try to find image
        while (matcher.find()) {
            // if image has been found get url by matcher group (2)
            val image = matcher.group(IMAGE_MATCHER_GROUP)
            val iTag = matcher.group(1) + matcher.group(IMAGE_MATCHER_GROUP) + matcher.group(5)

            // split all text with image url
            val test = descCopy?.split(iTag)

            if (test != null && test.size == 2) {
                separatedDesc.add(test[0])
                separatedDesc.add(image)
                separatedDesc.add(test[1])

                descCopy = test[1]
            }
        }

        if (separatedDesc.isEmpty()) {
            val tx = TextView(context)
            tx.setTextAppearance(context, R.style.ArticleBodyTextView)
            tx.setPadding(defHorMargin, defVerMargin, defHorMargin, defVerMargin)
            setTextViewHTML(tx, art.description!!)
            addView(tx, linearParams)
        } else {
            for (element in separatedDesc) {
                var view: View

                val m = pattern.matcher(element)
                if (!m.find()) {
                    view = TextView(context)
                    view.setTextAppearance(context, R.style.ArticleBodyTextView)
                    view.setPadding(defHorMargin, defVerMargin, defHorMargin, defVerMargin)
                    setTextViewHTML(view, element)
                } else {
                    view = ImageView(context)
                    Glide.with(this).load(element).into(view)
                }

                addView(view, linearParams)
            }
        }


        // Footer
        val footerView = inflate(context, R.layout.layout_article_footer, null)
        addView(footerView)
    }


    private fun setTextViewHTML(text: TextView, html: String) {
        val sequence = Html.fromHtml(html)
        val strBuilder = SpannableStringBuilder(sequence)
        val urls = strBuilder.getSpans(0, sequence.length, URLSpan::class.java)
        for (span in urls) {
            makeLinkClickable(strBuilder, span)
        }
        text.text = strBuilder
        text.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun makeLinkClickable(strBuilder: SpannableStringBuilder, span: URLSpan) {
        val start = strBuilder.getSpanStart(span)
        val end = strBuilder.getSpanEnd(span)
        val flags = strBuilder.getSpanFlags(span)
        val clickable = object : ClickableSpan() {
            override fun onClick(view: View) {
                val isIA = PreferenceManager.getDefaultSharedPreferences(context)
                        .getBoolean(context.getString(R.string.instant_articles_key), false)

                val browserIntent =
                        if (!isIA) Intent(Intent.ACTION_VIEW, Uri.parse(span.url))
                        else Intent(context, WebViewActivity::class.java)
                                .putExtra(ARTICLE_TITLE, span.url)
                                .putExtra(ARTICLE_URL, span.url)
                                .putExtra(FROM_SECONDARY_LINK, true)
                startActivity(context, browserIntent, null)
            }
        }
        strBuilder.setSpan(clickable, start, end, flags)
        strBuilder.removeSpan(span)
    }

}