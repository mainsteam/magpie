package ru.magpie.magpie.ui.fragment


import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import jp.wasabeef.recyclerview.animators.FadeInUpAnimator

import ru.magpie.magpie.R
import ru.magpie.magpie.domain.models.Article
import ru.magpie.magpie.mvp.presenter.FavorPresenter
import ru.magpie.magpie.mvp.view.FavorView
import ru.magpie.magpie.ui.activity.ArticleActivity
import ru.magpie.magpie.ui.adapter.ArticleAdapter


class FavorFragment : MvpAppCompatFragment(), FavorView {
    @InjectPresenter lateinit var presenter: FavorPresenter

    @BindView(R.id.articles_wrapper) lateinit var recycler: RecyclerView
    lateinit var adapter: ArticleAdapter
    lateinit var unbinder: Unbinder
    private val RIA_ADD_DURATION = 300L



    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater!!.inflate(R.layout.fragment_favor, container, false)
        unbinder = ButterKnife.bind(this, rootView)
        setUpRecycler()
        presenter.loadArticles()

        return rootView
    }


    private fun setUpRecycler() {
        recycler.layoutManager = LinearLayoutManager(context)
        recycler.itemAnimator = FadeInUpAnimator()
        recycler.itemAnimator.addDuration = RIA_ADD_DURATION
        adapter = ArticleAdapter(context, object : ArticleAdapter.OnItemClickListener {
            override fun onItemClick(item: Article) {
                showArticle(item)
            }
        })
        recycler.adapter = adapter
    }

    private fun showArticle(item: Article) {
        val intent = Intent(context, ArticleActivity::class.java)
        intent.putExtra(ArticleActivity.ARTICLE_ITEM, item)
        startActivity(intent)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder.unbind()
    }


    override fun showLoader() {
    }

    override fun onFavoritesLoaded(articles: ArrayList<Article>) {
        articles.forEach {
            println(it.favorite)
        }
        adapter.articles = articles.sortedByDescending { it.pubDate } as MutableList<Article>
        adapter.notifyDataSetChanged()
    }

    override fun onEmptyLoaded() {
    }


    override fun hideLoader() {
    }
}
