package ru.magpie.magpie.ui.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.design.widget.Snackbar
import android.support.v4.widget.NestedScrollView
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import butterknife.BindView
import butterknife.ButterKnife
import ru.magpie.magpie.R
import ru.magpie.magpie.domain.models.Article
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.view_channel_item.*
import ru.magpie.magpie.mvp.presenter.ArticlePresenter
import ru.magpie.magpie.mvp.view.ArticleView
import ru.magpie.magpie.ui.ArticleLayout
import ru.magpie.magpie.ui.activity.WebViewActivity.Companion.ARTICLE_TITLE
import ru.magpie.magpie.ui.activity.WebViewActivity.Companion.ARTICLE_URL
import ru.magpie.magpie.ui.activity.WebViewActivity.Companion.FROM_SECONDARY_LINK


class ArticleActivity : MvpAppCompatActivity(), ArticleView {
    @InjectPresenter lateinit var presenter: ArticlePresenter

    @BindView(R.id.root_view) lateinit var rootView: NestedScrollView
    @BindView(R.id.custom_toolbar) lateinit var toolbar: Toolbar
    var article: Article? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article)
        ButterKnife.bind(this)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24dp)
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        article = intent.getParcelableExtra(ARTICLE_ITEM)
        if (article != null) {

            supportActionBar?.title = "${article!!.acTitle}"
            val params = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT)
            rootView.addView(ArticleLayout(this, article!!), params)
            rootView.findViewById<Button>(R.id.read_more_btn).setOnClickListener {
                val isIA = PreferenceManager.getDefaultSharedPreferences(this)
                        .getBoolean(getString(R.string.instant_articles_key), false)

                val browserIntent =
                        if (!isIA) Intent(Intent.ACTION_VIEW, Uri.parse(article?.link))
                        else Intent(this, WebViewActivity::class.java)
                                .putExtra(ARTICLE_TITLE, article?.title)
                                .putExtra(ARTICLE_URL, article?.link)
                                .putExtra(FROM_SECONDARY_LINK, false)
                startActivity(browserIntent)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.article_top_menu, menu)
        val fav = menu?.getItem(1)
        val icon =
                if (article?.favorite!!) R.drawable.ic_bookmark_24dp
                else R.drawable.ic_bookmark_border_24dp
        fav?.setIcon(icon)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.favorite_btn -> {
                if (article?.favorite!!) {
                    presenter.deleteArtFromFavorite(article!!)
                    item.setIcon(R.drawable.ic_bookmark_border_24dp)
                    Snackbar.make(findViewById(android.R.id.content), "Article was deleted from favorite", Snackbar.LENGTH_LONG).setAction("Cancel", {
                        presenter.addArtInFavorite(article!!)
                        item.setIcon(R.drawable.ic_bookmark_24dp)
                    }).show()
                } else {
                    presenter.addArtInFavorite(article!!)
                    item.setIcon(R.drawable.ic_bookmark_24dp)
                    Toast.makeText(this, "Article was added to favorite", Toast.LENGTH_SHORT).show()
                }
            }
            R.id.share_btn -> {
                val shIntent = Intent(Intent.ACTION_SEND)
                shIntent.type = "text/plain"
                shIntent.putExtra(Intent.EXTRA_TEXT, article?.link)
                startActivity(Intent.createChooser(shIntent, "Share"))
            }
        }

        return true
    }

    companion object {
        val ARTICLE_ITEM = "article_item"
    }
}
