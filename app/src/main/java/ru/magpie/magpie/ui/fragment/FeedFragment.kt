package ru.magpie.magpie.ui.fragment


import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter

import ru.magpie.magpie.R
import ru.magpie.magpie.mvp.presenter.FeedPresenter
import ru.magpie.magpie.mvp.view.FeedView
import ru.magpie.magpie.domain.models.Article
import ru.magpie.magpie.ui.activity.ArticleActivity
import ru.magpie.magpie.ui.activity.ArticleActivity.Companion.ARTICLE_ITEM
import ru.magpie.magpie.ui.activity.MainActivity
import ru.magpie.magpie.ui.adapter.ArticleAdapter
import jp.wasabeef.recyclerview.animators.FadeInUpAnimator


class FeedFragment : MvpAppCompatFragment(), FeedView {
    @InjectPresenter lateinit var presenter: FeedPresenter

    private lateinit var unbinder: Unbinder
    @BindView(R.id.feed_recycler) lateinit var feedRecycler: RecyclerView
    @BindView(R.id.swiperefresh) lateinit var swipeRefresh: SwipeRefreshLayout
    lateinit var adapter: ArticleAdapter


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val rootView = inflater!!.inflate(R.layout.fragment_feed, container, false)
        unbinder = ButterKnife.bind(this, rootView)
        setUpRecycler()
        showLoader()
        presenter.loadFeeds(false)

        swipeRefresh.setOnRefreshListener {
            val size = adapter.articles.size
            adapter.articles.clear()
            adapter.notifyItemRangeRemoved(0, size)
            presenter.loadFeeds(true)
        }

        return rootView
    }


    private fun setUpRecycler() {
        feedRecycler.layoutManager = LinearLayoutManager(context)
        feedRecycler.itemAnimator = FadeInUpAnimator()
        feedRecycler.itemAnimator.addDuration = 300
        adapter = ArticleAdapter(context, object : ArticleAdapter.OnItemClickListener {
            override fun onItemClick(item: Article) {
                showArticle(item)
            }
        })
        feedRecycler.adapter = adapter
    }

    private fun showArticle(item: Article) {
        val intent = Intent(context, ArticleActivity::class.java)
        intent.putExtra(ARTICLE_ITEM, item)
        startActivity(intent)
    }

    override fun showLoader() = (activity as MainActivity).startPreloader()


    override fun hideLoader() = (activity as MainActivity).stopPreloader()


    override fun showListRefreshing() {
        swipeRefresh.isRefreshing = true
    }

    override fun hideListRefreshing() {
        swipeRefresh.isRefreshing = false
    }

    override fun onFeedLoaded(items: MutableList<Article>) {

    }

    override fun onItemLoaded(article: Article) {
        feedRecycler.scrollToPosition(0)
        adapter.addNewItem(article)

    }

    override fun showError(error: String) =
            Toast.makeText(context, error, Toast.LENGTH_SHORT).show()


    override fun onDestroyView() {
        super.onDestroyView()
        unbinder.unbind()
    }

}
