package ru.magpie.magpie.ui

import android.annotation.SuppressLint
import android.content.Context
import android.support.design.internal.BottomNavigationItemView
import android.support.design.internal.BottomNavigationMenuView
import android.support.design.widget.BottomNavigationView
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet
import android.util.Log
import android.view.Gravity
import android.widget.FrameLayout




class MyBottomNavigationView(context: Context, attrs: AttributeSet) : BottomNavigationView(context, attrs) {
    private val bottomMenuView: BottomNavigationMenuView?
        get() {
            var menuView: Any? = null
            try {
                val fd = BottomNavigationView::class.java.getDeclaredField("mMenuView")
                fd.isAccessible = true
                menuView = fd.get(this)
            } catch (e: NoSuchFieldException) {
                e.printStackTrace()
            } catch (e: IllegalAccessException) {
                e.printStackTrace()
            }

            return menuView as BottomNavigationMenuView?
        }

    init {
        //centerMenuIcon()
        BottomNavigationViewHelper().disableShiftMode(this)
    }

    @SuppressLint("RestrictedApi")
    private fun centerMenuIcon() {
        val menuView = bottomMenuView

        if (menuView != null) {
            for (i in 0 until menuView.childCount) {
                val menuItemView = menuView.getChildAt(i) as BottomNavigationItemView

                val icon = menuItemView.getChildAt(0) as AppCompatImageView

                val params = icon.layoutParams as FrameLayout.LayoutParams
                params.gravity = Gravity.CENTER

                menuItemView.setShiftingMode(true)
            }
        }
    }


    inner class BottomNavigationViewHelper() {

        @SuppressLint("RestrictedApi")
        fun disableShiftMode(view: BottomNavigationView) {
            val menuView = view.getChildAt(0) as BottomNavigationMenuView
            try {
                val shiftingMode = menuView.javaClass.getDeclaredField("mShiftingMode")
                shiftingMode.isAccessible = true
                shiftingMode.setBoolean(menuView, false)
                shiftingMode.isAccessible = false
                for (i in 0 until menuView.childCount) {
                    val item = menuView.getChildAt(i) as BottomNavigationItemView

                    item.setShiftingMode(false)
                    // set once again checked value, so view will be updated

                    item.setChecked(item.itemData.isChecked)
                }
            } catch (e: NoSuchFieldException) {
                Log.e("BNVHelper", "Unable to get shift mode field", e)
            } catch (e: IllegalAccessException) {
                Log.e("BNVHelper", "Unable to change value of shift mode", e)
            }

        }
    }
}