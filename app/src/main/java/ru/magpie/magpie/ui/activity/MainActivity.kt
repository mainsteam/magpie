package ru.magpie.magpie.ui.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.view.View
import android.widget.ProgressBar
import butterknife.BindView
import butterknife.ButterKnife
import ru.magpie.magpie.R
import ru.magpie.magpie.ui.fragment.FavorFragment
import ru.magpie.magpie.ui.fragment.FeedFragment
import ru.magpie.magpie.ui.fragment.SettingsFragment
import ru.magpie.magpie.ui.fragment.SubsFragment


class MainActivity : AppCompatActivity() {

    @BindView(R.id.bottomNavigationView) lateinit var bottomMenu: BottomNavigationView
    @BindView(R.id.progress_bar) lateinit var progressBar: ProgressBar


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        setUpBottomMenu()
        showFragment(FeedFragment())
    }


    private fun showFragment(fr: Fragment): Boolean {
        stopPreloader()

        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        }
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.main_container, fr)
        transaction.commit()
        return true
    }

    private fun setUpBottomMenu() {
        bottomMenu.setOnNavigationItemSelectedListener { item ->
            if (item.itemId != bottomMenu.selectedItemId) {
                when (item.itemId) {
                    R.id.item_favor -> showFragment(FavorFragment())
                    R.id.item_subs -> showFragment(SubsFragment())
                    R.id.item_settings -> showFragment(SettingsFragment())
                    else -> showFragment(FeedFragment())
                }
            } else {
                false
            }
        }
    }

    fun startPreloader() {
        if (!isFinishing && progressBar.visibility == View.GONE) {
            progressBar.visibility = View.VISIBLE
        }
    }

    fun stopPreloader() {
        if (!isFinishing && progressBar.visibility == View.VISIBLE) {
            progressBar.visibility = View.GONE
        }
    }
}
