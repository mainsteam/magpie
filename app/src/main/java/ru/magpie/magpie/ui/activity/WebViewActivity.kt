package ru.magpie.magpie.ui.activity

import android.annotation.SuppressLint
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import ru.magpie.magpie.R

class WebViewActivity : AppCompatActivity() {
    @BindView(R.id.web_view) lateinit var webView: WebView
    @BindView(R.id.custom_toolbar) lateinit var toolbar: Toolbar
    @BindView(R.id.browser_progress_bar) lateinit var progressBar: ProgressBar

    private val desctopBrowserUA =
            "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
    private var url: String? = null

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        ButterKnife.bind(this)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_24dp)
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        val fromSecondaryLink = intent.getBooleanExtra(FROM_SECONDARY_LINK, true)
        val title = intent.getStringExtra(ARTICLE_TITLE)
        url = intent.getStringExtra(ARTICLE_URL)

        supportActionBar?.title = ""
        supportActionBar?.subtitle = url

        webView.settings.javaScriptEnabled = true
        webView.webViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView, url: String) {
                supportActionBar?.title = view.title
                supportActionBar?.subtitle = url
                progressBar.visibility = View.GONE
                webView.visibility = View.VISIBLE
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                progressBar.visibility = View.VISIBLE
                webView.visibility = View.GONE
            }
        }
        webView.loadUrl(url)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.browser_top_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.open_with_chrome_btn -> {
                val chromeIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                startActivity(chromeIntent)
                finish()
            }
            R.id.copy_link_btn -> {
                val cpdManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val cpData = ClipData.newPlainText("URI", url)
                cpdManager.primaryClip = cpData
                Toast.makeText(this, "Link copied to clipboard", Toast.LENGTH_SHORT).show()
            }
        }

        return true
    }

    companion object {
        val ARTICLE_URL = "article_url"
        val ARTICLE_TITLE = "article_title"
        val FROM_SECONDARY_LINK = "from_secondary_link"
    }
}
