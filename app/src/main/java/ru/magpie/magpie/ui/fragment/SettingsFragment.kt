package ru.magpie.magpie.ui.fragment


import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v14.preference.SwitchPreference
import android.support.v7.preference.PreferenceFragmentCompat


import ru.magpie.magpie.R


class SettingsFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {
    private var shPref: SharedPreferences? = null

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.pref1)
        shPref = PreferenceManager.getDefaultSharedPreferences(activity)
        onSharedPreferenceChanged(shPref, getString(R.string.instant_articles_key))
    }

    override fun onResume() {
        super.onResume()
        preferenceScreen.sharedPreferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String) {
        val pref = findPreference(key)
        if (pref is SwitchPreference) {
            savePrefs(key, pref.isChecked)
        }
    }

    override fun onPause() {
        super.onPause()
        preferenceScreen.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this)
    }

    private fun savePrefs(key: String, value: Boolean?) {
        val editor = shPref?.edit()
        editor?.putBoolean(key, value!!)
        editor?.apply()
    }
}
