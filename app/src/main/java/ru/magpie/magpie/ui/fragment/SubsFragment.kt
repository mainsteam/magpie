package ru.magpie.magpie.ui.fragment


import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter

import ru.magpie.magpie.R
import ru.magpie.magpie.domain.models.Subscription
import ru.magpie.magpie.mvp.presenter.SubsPresenter
import ru.magpie.magpie.mvp.view.SubsView
import ru.magpie.magpie.ui.adapter.FeedlySearchAdapter


class SubsFragment : MvpAppCompatFragment(), SubsView {
    @InjectPresenter lateinit var presenter: SubsPresenter

    @BindView(R.id.subs_recycler) lateinit var subsRecycler: RecyclerView
    lateinit var adapter: FeedlySearchAdapter
    lateinit var unbinder: Unbinder
    val SEARCH_FRAGMENT_TAG = "searchFragment"


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater!!.inflate(R.layout.fragment_subs, container, false)
        unbinder = ButterKnife.bind(this, rootView)
        setUpRecycler()
        presenter.loadSubs()
        return rootView
    }

    override fun onSubsLoaded(feeds: MutableList<Subscription>) {
        adapter.items = feeds
        adapter.notifyDataSetChanged()
    }


    @OnClick(R.id.add_feed_fab)
    fun searchFeed(view: View) {
        fragmentManager.beginTransaction().add(R.id.main_container, SearchFeedFragment())
                .addToBackStack(SEARCH_FRAGMENT_TAG)
                .commit()
    }

    private fun setUpRecycler() {
        subsRecycler.layoutManager = LinearLayoutManager(context)
        adapter = FeedlySearchAdapter(context)
        subsRecycler.adapter = adapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder.unbind()
    }

}
