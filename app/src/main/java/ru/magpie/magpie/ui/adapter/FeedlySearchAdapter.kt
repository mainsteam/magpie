package ru.magpie.magpie.ui.adapter

import android.content.Context
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import ru.magpie.magpie.R
import ru.magpie.magpie.common.MyApp
import ru.magpie.magpie.domain.dao.SubsDao
import ru.magpie.magpie.domain.models.Subscription
import javax.inject.Inject
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers


class FeedlySearchAdapter(val context: Context) : RecyclerView.Adapter<FeedlySearchAdapter.ViewHolder>() {

    @Inject lateinit var subsDao: SubsDao
    var items: MutableList<Subscription> = mutableListOf()

    init {
        MyApp.appComponent.inject(this)
    }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.view_search_feed_item, parent, false))

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val channel = items[position]

        holder?.feedlyItemTitle?.text = channel.title
        holder?.feedlyItemUrl?.text = channel.feedId.replaceFirst("feed/".toRegex(), "")

        holder?.itemView?.setOnClickListener {
        }

        holder?.feedAcSettings?.setOnClickListener {
            val popup = PopupMenu(context, holder.feedAcSettings)
            popup.inflate(R.menu.feed_ac_popup_menu)
            popup.menu.findItem(R.id.ac_subscription_item).title =
                    if (channel.isSubscribed) "Unsubscribe" else "Subscribe"

            popup.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.ac_subscription_item -> {
                        if (!channel.isSubscribed) {
                            Single.fromCallable {
                                channel.isSubscribed = true
                                subsDao.insertSub(channel)
                            }
                                    .subscribeOn(Schedulers.io())
                                    .subscribe()
                        } else {
                            Single.fromCallable {
                                subsDao.deleteSub(channel)
                            }
                                    .subscribeOn(Schedulers.io())
                                    .subscribe()
                        }
                        true
                    }
                    else -> false
                }
            }
            popup.show()
        }


        if (channel.iconUrl != null) {
            Glide.with(context).load(channel.iconUrl).into(holder?.feedlyItemImage)
            holder?.feedlyItemImage?.visibility = View.VISIBLE
        } else {
            Glide.with(context).clear(holder?.feedlyItemImage)
            holder?.feedlyItemImage?.visibility = View.GONE
        }
    }


    class ViewHolder(viewItem: View) : RecyclerView.ViewHolder(viewItem) {
        var feedlyItemTitle: TextView = viewItem.findViewById(R.id.feedly_item_title)
        var feedlyItemUrl: TextView = viewItem.findViewById(R.id.feedly_search_url)
        var feedlyItemImage: ImageView = viewItem.findViewById(R.id.feedly_item_image)
        var feedAcSettings: View = viewItem.findViewById(R.id.feed_account_settings)
    }
}