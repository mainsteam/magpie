package ru.magpie.magpie.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import ru.magpie.magpie.R
import ru.magpie.magpie.common.HtmlHelper
import ru.magpie.magpie.domain.models.Article
import java.util.*
import java.util.regex.Pattern


class ArticleAdapter(private val context: Context, private val listener: OnItemClickListener) :
        RecyclerView.Adapter<ArticleAdapter.ViewHolder>() {

    private val SITE_IMAGE = "/favicon.ico"
    private val pattern = Pattern.compile("http.+?//[^/]*")
    private val htmlHelper = HtmlHelper()

    var articles: MutableList<Article> = arrayListOf()

    // Add item using binary search
    fun addNewItem(item: Article) {
        var left = 0
        var right = articles.size
        var mid: Int

        while (left < right) {
            mid = (left + right) / 2

            val res = if (articles[mid].pubDate?.time!! < item.pubDate?.time!!) 1 else 0
            if (res == 1) right = mid
            else left = mid + 1
        }

        articles.add(left, item)
        notifyItemInserted(left)
    }


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent!!.context)
                    .inflate(R.layout.view_channel_item, parent, false))

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val article = articles[position]

        val time: Long = (Date().time - article.pubDate?.time!!) / (1000 * 60)
        holder?.pubDate?.text = getPostedTime(time)
        holder?.itemTitle?.text = article.title

        // replace all special html characters with unicode
        val rDesc = htmlHelper.correct(article.description!!)
        holder?.itemBody?.text = rDesc
        holder?.acNameWithDate?.text = article.acTitle
        val iUrlMatcher = pattern.matcher(article.acLink)
        val iUrl: String? = if (iUrlMatcher.find()) {
            iUrlMatcher.group(0)
        } else {
            article.acLink
        }
        Glide.with(context).load(iUrl + SITE_IMAGE).into(holder?.acImage)

        if (article.imageUrl != null) {
            Glide.with(context)
                    .load(article.imageUrl)
                    .into(holder?.itemImage)
            holder?.itemImage?.visibility = View.VISIBLE
        } else {
            Glide.with(context).clear(holder?.itemImage)
            holder?.itemImage?.visibility = View.GONE
        }

        holder?.itemView?.setOnClickListener { listener.onItemClick(article) }
    }


    private fun getPostedTime(minutes: Long): CharSequence? = when {
        minutes < 0 -> "0m"
        minutes < 60 -> "${minutes}m"
        minutes / 60 < 24 -> "${minutes / 60}h"
        else -> "${minutes / (60 * 24)}d"
    }


    override fun getItemCount(): Int = articles.size


    class ViewHolder(viewItem: View) : RecyclerView.ViewHolder(viewItem) {
        var itemTitle: TextView = viewItem.findViewById(R.id.channel_item_title)
        var itemBody: TextView = viewItem.findViewById(R.id.channel_item_body)
        var itemImage: ImageView = viewItem.findViewById(R.id.channel_item_image)
        var acImage: ImageView = viewItem.findViewById(R.id.account_icon)
        var acNameWithDate: TextView = viewItem.findViewById(R.id.account_name)
        var pubDate: TextView = viewItem.findViewById(R.id.article_date)
    }


    interface OnItemClickListener {
        fun onItemClick(item: Article)
    }
}