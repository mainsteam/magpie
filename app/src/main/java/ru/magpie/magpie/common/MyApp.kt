package ru.magpie.magpie.common

import android.app.Application
import ru.magpie.magpie.di.component.AppComponent
import ru.magpie.magpie.di.component.DaggerAppComponent
import ru.magpie.magpie.di.module.AppModule
import ru.magpie.magpie.di.module.DataModule


class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initComponent()
    }

    private fun initComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .dataModule(DataModule(applicationContext))
                .build()
    }

    companion object {
        lateinit var appComponent: AppComponent
    }
}