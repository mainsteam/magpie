package ru.magpie.magpie.common

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import javax.inject.Inject


class NetworkManager {

    @Inject lateinit var context: Context

    init {
        MyApp.appComponent.inject(this)
    }

    fun isConnectionOk(): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
        return (networkInfo != null && networkInfo.isConnected)
    }
}