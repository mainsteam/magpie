package ru.magpie.magpie.common

import android.animation.Animator
import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.content.Context
import android.os.Build
import android.view.View
import android.support.annotation.RequiresApi
import android.view.ViewAnimationUtils
import android.support.v4.view.animation.FastOutSlowInInterpolator
import android.util.TypedValue
import android.view.animation.AccelerateDecelerateInterpolator


class AnimUtils {

    fun registerRevealAnimation(context: Context, view: View,
                                startColor: Int, endColor: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view.addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun onLayoutChange(v: View?, left: Int, top: Int, right: Int, bottom: Int,
                                            oldLeft: Int, oldTop: Int, oldRight: Int, oldBottom: Int) {
                    v?.removeOnLayoutChangeListener(this)
                    val width = view.width
                    val height = view.height
                    val cx = width - dpToPx(context, 46F).toInt()
                    val cy = height - dpToPx(context, 46F).toInt()
                    val duration = context.resources.getInteger(android.R.integer.config_longAnimTime).toLong()

                    val finalRadius = Math.sqrt((width * width + height * height).toDouble()).toFloat()
                    val anim = ViewAnimationUtils.createCircularReveal(v, cx, cy, 0f, finalRadius).setDuration(duration)
                    anim.interpolator = FastOutSlowInInterpolator()
                    anim.start()
                    startColorAnimation(view, startColor, endColor, duration)
                }
            })
        }
    }


    fun startColorAnimation(view: View, startColor: Int, endColor: Int, duration: Long) {
        val anim = ValueAnimator()
        anim.setIntValues(startColor, endColor)
        anim.setEvaluator(ArgbEvaluator())
        anim.addUpdateListener { valueAnimator ->
            view.setBackgroundColor(valueAnimator.animatedValue as Int)
        }
        anim.duration = duration
        anim.start()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun revealView(view: View, duration: Long, startRadius: Float): Animator {
        val cx = view.measuredWidth
        val cy = view.measuredHeight

        val finalRadius = Math.max(view.width, view.height)

        val anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, startRadius, finalRadius.toFloat())
        anim.duration = duration
        anim.interpolator = AccelerateDecelerateInterpolator()

        return anim
    }



    fun dpToPx(context: Context, valueInDp: Float): Float {
        val metrics = context.resources.displayMetrics
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics)
    }
}