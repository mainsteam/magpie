package ru.magpie.magpie.mvp.view

import com.arellomobile.mvp.MvpView
import ru.magpie.magpie.domain.models.Subscription


interface SearchFeedView : MvpView {
    fun onSearchFinished(feeds: MutableList<Subscription>)
}