package ru.magpie.magpie.mvp.presenter


import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.magpie.magpie.common.MyApp
import ru.magpie.magpie.domain.dao.SubsDao
import ru.magpie.magpie.mvp.view.FeedView
import ru.magpie.magpie.domain.models.Article
import ru.magpie.magpie.domain.RssFetcher
import javax.inject.Inject


@InjectViewState
class FeedPresenter : MvpPresenter<FeedView>() {
    @Inject lateinit var rssFetcher: RssFetcher
    @Inject lateinit var subsDao: SubsDao

    var allArticles: MutableList<Article> = arrayListOf()

    init {
        MyApp.appComponent.inject(this)
    }

    fun loadFeeds(refresh: Boolean) {
        subsDao.getAllSubs()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .map { urls -> urls.map { it.feedId.replaceFirst("feed/".toRegex(), "") } }
                .subscribe { subs ->
                    rssFetcher.requestFeeds(subs)
                            .subscribeOn(Schedulers.newThread())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ feed ->
                                if (refresh) viewState.hideListRefreshing()
                                else viewState.hideLoader()
                                feed?.articles?.forEach {
                                    viewState.onItemLoaded(it)
                                }
                            }, {
                                if (refresh) viewState.hideListRefreshing()
                                else viewState.hideLoader()
                                viewState.showError(it.message!!)
                            })
                }
    }


    @Synchronized private fun mergeArticles(newArticles: MutableList<Article>) {
        allArticles.addAll(newArticles)
        viewState.onFeedLoaded(allArticles.sortedByDescending { it.pubDate } as MutableList<Article>)
    }
}