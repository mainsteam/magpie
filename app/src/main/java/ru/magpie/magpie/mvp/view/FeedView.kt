package ru.magpie.magpie.mvp.view

import ru.magpie.magpie.domain.models.Article


interface FeedView : LoadingView {
    fun onFeedLoaded(items: MutableList<Article>)
    fun showError(error: String)
    fun showListRefreshing()
    fun hideListRefreshing()
    fun onItemLoaded(article: Article)
}