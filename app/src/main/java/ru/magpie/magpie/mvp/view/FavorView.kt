package ru.magpie.magpie.mvp.view

import ru.magpie.magpie.domain.models.Article


interface FavorView: LoadingView {
    fun onFavoritesLoaded(articles: ArrayList<Article>)
    fun onEmptyLoaded()
}