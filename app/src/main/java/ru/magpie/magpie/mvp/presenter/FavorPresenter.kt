package ru.magpie.magpie.mvp.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.magpie.magpie.common.MyApp
import ru.magpie.magpie.domain.dao.ArticlesDao
import ru.magpie.magpie.domain.models.Article
import ru.magpie.magpie.mvp.view.FavorView
import javax.inject.Inject


@InjectViewState
class FavorPresenter : MvpPresenter<FavorView>() {
    @Inject lateinit var artDao: ArticlesDao

    init {
        MyApp.appComponent.inject(this)
    }

    fun loadArticles() {
        artDao.getFavoriteArticles()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { articles ->
                    if (!articles.isEmpty()) {
                        viewState.onFavoritesLoaded(articles as ArrayList<Article>)
                    } else {
                        viewState.onEmptyLoaded()
                    }
                }
    }

}