package ru.magpie.magpie.mvp.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import ru.magpie.magpie.common.MyApp
import ru.magpie.magpie.domain.dao.ArticlesDao
import ru.magpie.magpie.domain.models.Article
import ru.magpie.magpie.mvp.view.ArticleView
import javax.inject.Inject


@InjectViewState
class ArticlePresenter : MvpPresenter<ArticleView>() {
    @Inject lateinit var artDao: ArticlesDao

    init {
        MyApp.appComponent.inject(this)
    }

    fun addArtInFavorite(article: Article) {
        article.favorite = true
        Single.fromCallable {
            artDao.insertArticle(article)
        }
                .subscribeOn(Schedulers.io())
                .subscribe()
    }

    fun deleteArtFromFavorite(article: Article) {
        Single.fromCallable {
            artDao.deleteArticle(article)
        }
                .subscribeOn(Schedulers.io())
                .subscribe()
    }
}