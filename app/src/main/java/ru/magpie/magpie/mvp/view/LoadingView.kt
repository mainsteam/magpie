package ru.magpie.magpie.mvp.view

import com.arellomobile.mvp.MvpView


interface LoadingView : MvpView {
    fun showLoader()
    fun hideLoader()
}