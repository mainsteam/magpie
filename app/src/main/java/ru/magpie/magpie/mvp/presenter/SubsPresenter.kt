package ru.magpie.magpie.mvp.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ru.magpie.magpie.common.MyApp
import ru.magpie.magpie.domain.dao.SubsDao
import ru.magpie.magpie.domain.models.Subscription
import ru.magpie.magpie.mvp.view.SubsView
import javax.inject.Inject


@InjectViewState
class SubsPresenter : MvpPresenter<SubsView>() {
    @Inject lateinit var subsDao: SubsDao
    var disposable: Disposable? = null

    init {
        MyApp.appComponent.inject(this)
    }


    fun loadSubs() {
        disposable = subsDao.getAllSubs()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { list ->
                    viewState.onSubsLoaded(list as MutableList<Subscription>)
                }
    }


    override fun onDestroy() {
        super.onDestroy()
        if (disposable != null) {
            disposable?.dispose()
        }
    }

}