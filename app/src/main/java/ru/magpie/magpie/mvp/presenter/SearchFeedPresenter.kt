package ru.magpie.magpie.mvp.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import ru.magpie.magpie.common.MyApp
import ru.magpie.magpie.domain.RssFetcher
import ru.magpie.magpie.mvp.view.SearchFeedView
import javax.inject.Inject


@InjectViewState
class SearchFeedPresenter : MvpPresenter<SearchFeedView>() {
    @Inject lateinit var rssFetcher: RssFetcher

    init {
        MyApp.appComponent.inject(this)
    }

    fun searchFeed(keyword: String) {
        rssFetcher.searchFeed(keyword)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { list ->
                    viewState.onSearchFinished(list)
                }
    }

}