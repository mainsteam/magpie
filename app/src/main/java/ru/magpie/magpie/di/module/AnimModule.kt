package ru.magpie.magpie.di.module

import dagger.Module
import dagger.Provides
import ru.magpie.magpie.common.AnimUtils
import javax.inject.Singleton

@Module
class AnimModule {
    @Singleton @Provides
    fun provideAnimUtils(): AnimUtils = AnimUtils()
}