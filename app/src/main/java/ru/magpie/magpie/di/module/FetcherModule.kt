package ru.magpie.magpie.di.module

import dagger.Module
import dagger.Provides
import ru.magpie.magpie.domain.RssFetcher
import javax.inject.Singleton

@Module
class FetcherModule {
    @Singleton @Provides
    fun provideRssFetcher(): RssFetcher = RssFetcher()
}