package ru.magpie.magpie.di.component

import dagger.Component
import ru.magpie.magpie.common.NetworkManager
import ru.magpie.magpie.di.module.*
import ru.magpie.magpie.mvp.presenter.*
import ru.magpie.magpie.ui.adapter.FeedlySearchAdapter
import ru.magpie.magpie.ui.fragment.SearchFeedFragment
import javax.inject.Singleton


@Singleton
@Component(modules = [(AppModule::class), (AnimModule::class), (FetcherModule::class),
    (DataModule::class)])
interface AppComponent {

    fun inject(manager: NetworkManager)

    fun inject(fragment: SearchFeedFragment)

    fun inject(presenter: FeedPresenter)
    fun inject(presenter: SearchFeedPresenter)
    fun inject(presenter: ArticlePresenter)
    fun inject(presenter: FavorPresenter)
    fun inject(presenter: SubsPresenter)

    fun inject(adapter: FeedlySearchAdapter)
}