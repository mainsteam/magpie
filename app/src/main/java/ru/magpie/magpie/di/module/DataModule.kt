package ru.magpie.magpie.di.module


import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import ru.magpie.magpie.domain.AppDatabase
import ru.magpie.magpie.domain.dao.ArticlesDao
import ru.magpie.magpie.domain.dao.SubsDao
import javax.inject.Singleton


@Module
class DataModule(context: Context) {
    private var appDatabase: AppDatabase =
            Room.databaseBuilder(context, AppDatabase::class.java, "test_db")
                    //.addMigrations(MIGRATION_1_2)
                    .fallbackToDestructiveMigration()
                    .build()

    @Singleton @Provides
    fun provideDatabase(): AppDatabase = appDatabase


    @Singleton @Provides
    fun provideSubsDao(): SubsDao = appDatabase.getSubsDao()


    @Singleton @Provides
    fun provideFavorsDao(): ArticlesDao = appDatabase.getFavorsDao()
}