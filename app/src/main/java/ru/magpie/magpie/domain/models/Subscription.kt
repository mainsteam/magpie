package ru.magpie.magpie.domain.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable


/**
 *  Модель, сериализующая ответ API cloud.feedly.com на запрос поиска
 * */
@Entity
class Subscription : Serializable {
    @PrimaryKey @SerializedName("feedId") lateinit var feedId: String
    @SerializedName("title") var title: String? = null
    @SerializedName("website") var website: String? = null
    @SerializedName("description") var description: String? = null
    @SerializedName("iconUrl") var iconUrl: String? = null
    @SerializedName("visualUrl") var visualUrl: String? = null
    var isSubscribed: Boolean = false
}