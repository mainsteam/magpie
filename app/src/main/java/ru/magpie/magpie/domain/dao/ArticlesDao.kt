package ru.magpie.magpie.domain.dao

import android.arch.persistence.room.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Flowable
import ru.magpie.magpie.domain.models.Article
import java.util.*


@Dao
interface ArticlesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertArticle(art: Article)

    @Delete
    fun deleteArticle(art: Article)

    @Query("SELECT * FROM article")
    fun getAllArticles(): Flowable<List<Article>>

    @Query("SELECT * FROM article WHERE favorite ")
    fun getFavoriteArticles(): Flowable<List<Article>>


    class Converters {


        @TypeConverter
        fun fromString(value: String): ArrayList<String> {
            val listType = object : TypeToken<ArrayList<String>>() {}.type
            return Gson().fromJson<ArrayList<String>>(value, listType)
        }

        @TypeConverter
        fun fromArrayList(list: ArrayList<String>): String {
            val gson = Gson()
            return gson.toJson(list)
        }

        @TypeConverter
        fun fromDate(value: Date): Long = value.time


        @TypeConverter
        fun fromDateTime(time: Long): Date = Date(time)
    }

}