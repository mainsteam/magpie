package ru.magpie.magpie.domain.models

import com.rometools.rome.feed.synd.SyndEntryImpl
import com.rometools.rome.feed.synd.SyndFeed
import android.os.Parcelable
import android.os.Parcel


class Feed() : Parcelable {
    var articles: MutableList<Article> = mutableListOf()
    var description: String? = null
    var copyright: String? = null
    var generator: String? = null
    var encoding: String? = null
    var language: String? = null
    var imgUrl: String? = null
    var author: String? = null
    var title: String? = null
    var link: String? = null


    constructor(syndFeed: SyndFeed) : this() {
        description = syndFeed.description
        copyright = syndFeed.copyright
        encoding = syndFeed.encoding
        language = syndFeed.language
        imgUrl = syndFeed.image.url
        author = syndFeed.author
        title = syndFeed.title
        link = syndFeed.link
        syndFeed.entries.forEach {
            articles.add(Article(title, link, it as SyndEntryImpl))
        }
    }


    constructor(parcel: Parcel) : this() {
        description = parcel.readString()
        generator = parcel.readString()
        copyright = parcel.readString()
        encoding = parcel.readString()
        language = parcel.readString()
        imgUrl = parcel.readString()
        author = parcel.readString()
        title = parcel.readString()
        link = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(description)
        parcel.writeString(copyright)
        parcel.writeString(generator)
        parcel.writeString(encoding)
        parcel.writeString(language)
        parcel.writeString(imgUrl)
        parcel.writeString(author)
        parcel.writeString(title)
        parcel.writeString(link)
    }

    override fun describeContents(): Int = 0


    companion object CREATOR : Parcelable.Creator<Feed> {
        override fun createFromParcel(parcel: Parcel): Feed {
            return Feed(parcel)
        }

        override fun newArray(size: Int): Array<Feed?> {
            return arrayOfNulls(size)
        }
    }
}