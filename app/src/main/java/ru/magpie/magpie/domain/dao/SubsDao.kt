package ru.magpie.magpie.domain.dao

import android.arch.persistence.room.*
import io.reactivex.Flowable
import ru.magpie.magpie.domain.models.Subscription


@Dao
interface SubsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(subs: List<Subscription>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSub(sub: Subscription)

    @Delete
    fun deleteSub(sub: Subscription)

    @Query("SELECT * FROM subscription")
    fun getAllSubs(): Flowable<List<Subscription>>
}