package ru.magpie.magpie.domain

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.arch.persistence.room.migration.Migration
import ru.magpie.magpie.domain.dao.ArticlesDao
import ru.magpie.magpie.domain.dao.SubsDao
import ru.magpie.magpie.domain.models.Article
import ru.magpie.magpie.domain.models.Subscription


@Database(entities = [(Subscription::class), (Article::class)], version = 1)
@TypeConverters(value = [(ArticlesDao.Converters::class)])
abstract class AppDatabase : RoomDatabase() {

    abstract fun getSubsDao(): SubsDao

    abstract fun getFavorsDao(): ArticlesDao


    companion object {

        val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE article (\ntitle TEXT, \ndescription TEXT, \nlink TEXT, \nfavorite INTEGER NOT NULL, \npubDate INTEGER NOT NULL, \nimageUrl TEXT, \nuid INTEGER NOT NULL, \nacLink TEXT, \nacTitle TEXT, \nauthor TEXT, \ncategories TEXT, \nseparatedDesc TEXT)")
            }
        }
    }
}