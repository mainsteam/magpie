package ru.magpie.magpie.domain.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.rometools.rome.feed.synd.SyndEntryImpl
import android.os.Parcelable
import android.os.Parcel
import ru.magpie.magpie.common.HtmlHelper
import java.util.*


@Entity
class Article() : Parcelable {
    @Ignore private val IMAGE_MATCHER_GROUP = 2
    @Ignore private val htmlHelper = HtmlHelper()

    @PrimaryKey lateinit var link: String
    var categories: ArrayList<String> = arrayListOf()
    var description: String? = null
    var favorite: Boolean = false
    var imageUrl: String? = null
    var acTitle: String? = null
    var author: String? = null
    var acLink: String? = null
    var pubDate: Date? = null
    var title: String? = null

    constructor(parcel: Parcel) : this() {
        link = parcel.readString()
        description = parcel.readString()
        favorite = parcel.readByte() != 0.toByte()
        imageUrl = parcel.readString()
        acTitle = parcel.readString()
        author = parcel.readString()
        acLink = parcel.readString()
        pubDate = Date(parcel.readLong())
        title = parcel.readString()
    }


    constructor(acTitle: String?, acLink: String?, art: SyndEntryImpl) : this() {
        this.acTitle = acTitle
        this.acLink = acLink
        title = art.title
        author = art.author
        link = art.link
        pubDate = art.publishedDate
        description = try {
            if (art.description != null) {
                art.description.value
            } else {
                throw NullPointerException()
            }
        } catch (e: NullPointerException) {
            art.contents.first().value
        }

        imageUrl = htmlHelper.findImg(description, IMAGE_MATCHER_GROUP)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(link)
        parcel.writeString(description)
        parcel.writeByte(if (favorite) 1 else 0)
        parcel.writeString(imageUrl)
        parcel.writeString(acTitle)
        parcel.writeString(author)
        parcel.writeString(acLink)
        parcel.writeLong(pubDate?.time!!)
        parcel.writeString(title)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Article> {
        override fun createFromParcel(parcel: Parcel): Article {
            return Article(parcel)
        }

        override fun newArray(size: Int): Array<Article?> {
            return arrayOfNulls(size)
        }
    }
}