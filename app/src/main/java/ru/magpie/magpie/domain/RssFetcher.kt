package ru.magpie.magpie.domain


import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.rometools.rome.io.SyndFeedInput
import com.rometools.rome.io.XmlReader
import io.reactivex.Observable
import okhttp3.*
import java.io.IOException
import org.json.JSONObject
import ru.magpie.magpie.domain.models.Feed
import ru.magpie.magpie.domain.models.Subscription


class RssFetcher {
    val TAG = "RssFetcher"
    //  URL для запроса к API cloud.feedly.com на поиск фида
    private val FEEDLY_API_SEARCH_FEED = "https://cloud.feedly.com/v3/search/feeds?n=10&q="
    private val FEEDLY_RESULTS = "results"

    private val client = OkHttpClient()
    private lateinit var request: Request


    fun requestFeeds(urls: List<String>): Observable<Feed> =
            Observable.create<Feed> { o ->
                urls.forEach { url ->
                    initRequest(url)

                    try {
                        val resp = client.newCall(request).execute()
                        val feed = Feed(SyndFeedInput().build(XmlReader(resp?.body()?.byteStream())))
                        o.onNext(feed)
                    } catch (e: Exception) {
                        Log.e(TAG, e.message)
                    }
                }
            }


    private fun initRequest(url: String) {
        request = Request.Builder()
                .url(url)
                .build()
    }


    fun searchFeed(keyword: String): Observable<MutableList<Subscription>> =
            Observable.create({
                request = Request.Builder()
                        .url(FEEDLY_API_SEARCH_FEED + keyword)
                        .build()

                client.newCall(request).enqueue(object : Callback {
                    override fun onResponse(call: Call?, response: Response?) {
                        if (response != null && response.isSuccessful) {
                            val respJson = JSONObject(response.body()?.string())
                            val results = respJson.getJSONArray(FEEDLY_RESULTS)

                            val listType = object : TypeToken<ArrayList<Subscription>>() {}.type
                            val channels: MutableList<Subscription> = Gson().fromJson(results.toString(), listType)

                            it.onNext(channels)
                        }
                    }

                    override fun onFailure(call: Call?, e: IOException?) {
                        it.onError(Throwable(e))
                    }
                })
            })
}